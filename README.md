<!--- OASIS_START --->
<!--- DO NOT EDIT (digest: aeba99166ba4c32600accdc445d7b687) --->

btw - This program help you to backup your Taskwarrior list
===========================================================

See the file [INSTALL.md](INSTALL.md) for building and installation
instructions.

Copyright and license
---------------------

btw is distributed under the terms of the CEA-CNRS-INRIA Logiciel Libre,
BSD-like.

<!--- OASIS_STOP --->
